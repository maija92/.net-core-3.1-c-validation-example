﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using TestTaskWalrus.Validation;

namespace TestTaskWalrus.Models
{
    public class FindTextInFile
    {
        [Required]
        [StringLength(255, MinimumLength = 3)]
        public string Text { get; set; }

        [Required]
        [AllowedExtensions(new string[] { ".txt", ".json", ".csv", ".xml", ".css", ".html", ".js" })]
        public IFormFile File { set; get; }
    }
}
