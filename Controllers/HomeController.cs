﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestTaskWalrus.Models;

namespace TestTaskWalrus.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(FindTextInFile validateText)
        {
            if (!ModelState.IsValid)
            {
                return View(validateText);
            }
            else
            {
                using StreamReader sr = new StreamReader(validateText.File.OpenReadStream());
                String line = sr.ReadToEnd();

                if (line.Contains(validateText.Text))
                {
                    ViewData["UserMessage"] = "<div class='alert alert-success mt-3' role='alert'>Success!</div>";
                }
                else
                {
                    ViewData["UserMessage"] = "<div class='alert alert-warning mt-3' role='alert'>Are you sure it is correct file?</div>";
                }

                return View();
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
